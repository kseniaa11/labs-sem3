
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include <iostream>
#include <limits>
#include <math.h>

int number(double& n);

class Bern	// ����������� ��������
{
private:
	double c;

public:
	Bern() {
		c = 1;
	};
	Bern(double par); 			// �����������
	~Bern() {};						// ����������
	double set(double newc);			// ��������� ��������� ������
	double get(void) const         // ������� ��������� ������
	{		
		return c;
	};

	double ro(double fi) const;		// ���������� ���������� �� �
	double rad1(double fi) const;		// ������ �������� �� ����
	double rad2(double ro) const;		// ������ �������� �� ��
	double area(double fi) const;			// ������� �������
	double allarea(void) const;			// ������� ������

};

