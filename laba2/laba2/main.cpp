﻿
#include <iostream>
#include "lem.h"
#include <math.h>

int main()
{
	double par;
	Bern B();
	while (par != -1) {
		std::cout << "Enter parametr (par > 0). -1 for the end.\n";
		number(par);
		try 
		{
			B.set(par);
		}
		catch (const std::exception&) 
		{
			std::cout << "Failed to set such parameter." << std::endl;
			continue;
		}

		std::cout << "Choose function:\n" << "1. ro to (0,0) from angle\n" << "2. R from angle\n" << "3. R from ro\n"
			<< "4. S from angle\n" << "5. S all object\n";
		
		double fi;
		double ro;
		int ch;
		std::cin >> ch;
		switch (ch) 
		{
		case 1:
			std::cout << "Enter angle (in rad).\n";
			number(fi);
			std::cout << "ro to (0,0) from angle: ";
			try
				B.ro(fi);
			catch (const std::exception&)
				std::cout << "Failed to set such parameter." << std::endl;
			std::cout << "\n";
			break;
		case 2:
			std::cout << "Enter angle (in rad).\n";
			number(fi);
			std::cout << "R from angle: ";
			try
				B.rad1(fi);
			catch (const std::exception&)
				std::cout << "Failed to set such parameter." << std::endl;
			std::cout << "\n";
			break;
		case 3:
			std::cout << "Enter ro for finding R from ro.\n";
			number(ro);
			std::cout << "R from ro: ";
			try
				B.rad2(ro);
			catch (const std::exception&)
				std::cout << "Failed to set such parameter." << std::endl;
			std::cout << "\n";
			break;
		case 4:
			std::cout << "Enter angle (in rad).\n";
			number(fi);
			std::cout << "S from angle: " << B.area(fi) << "\n";
			break;
		case 5:
			std::cout << "S all object: " << B.allarea() << "\n";
			break;
		}

	}
	
	

    _CrtDumpMemoryLeaks();
    return 0;
}

/*Вариант 9. Лемниската Бернулли
Разработать класс, определяющий кривую – лемнискату Бернулли.
Лемниската Бернулли – геометрическое место точек M, для которых произведение расстоянии MF1* MF2 = с2 расстояний до концов данного отрезка F1F2 = 2c равно четверти квадрата длины этого отрезка.
1) Определить состояние класса.
2) Разработать необходимые конструкторы и методы получения и изменения параметров, определяющих кривую.
3) Вернуть расстояние до центра в полярной системе координат в зависимости от угла для точки принадлежащей лемнискате.
4) Вернуть радиуса кривизны в зависимости от угла полярного радиуса.
5) Вернуть радиуса кривизны в зависимости от длины полярного радиуса.
6) Вернуть площадь полярного сектора лемнискаты в зависимости от угла полярного радиуса.
7) Вернуть площадь лемнискаты.
Разработать диалоговую программу для тестирования класса.*/
