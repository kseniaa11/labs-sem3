
// ������ ������
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>
// ����
#include <iostream>
#include <limits>
#include "lem.h"

#include <math.h>

const double pi = 3.14159;

int number(double& n) 
{
	using std::cin;
	cin >> n;
	while (!cin.good())
	{
		cin.clear();
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		cin.clear();
		cin >> n;
	}
	return 1;
}

Bern::Bern(double par) 
{
	c = set(par);
}



double Bern::set(double newc) 
{
	if (newc > 0) 
	{
		c = newc;
	}
	else 
	{
		throw std::exception("Negative parametr.");
	}
	return c;
}

double Bern::ro(double fi) const		// ���������� ���������� �� �
{
	if ((fi > pi / 4 && fi < 3 * pi / 4) || (fi > 5 * pi / 4 && fi < 7 * pi / 4))
	{
		throw std::exception("Wrong angle.");
	}
	return (c * sqrt(2 * cos(2 * fi)));
}

double Bern::rad1(double fi) const		// ������ �������� �� ����
{
	if ((fi > pi / 4 && fi < 3 * pi / 4) || (fi > 5 * pi / 4 && fi < 7 * pi / 4))
	{
	throw std::exception("Wrong angle.");
	}

	double r = c * sqrt(2 * cos(2 * fi));
	return (r / (3 * cos(2 * fi)));
}

double Bern::rad2(double ro) const		// ������ �������� �� ��
{
	if ((ro > c * sqrt(2)) || (ro < -c * sqrt(2)))
	{
		throw std::exception("Wrong ro.");
	}
	return ((2 * c * c) / (3 * ro));
}

double Bern::area(double fi) const			// ������� �������
{
	
	while (fi >= 2 * pi)
		fi = fi - 2 * pi;
	if (fi > pi / 4 && fi < 3 * pi / 4)
		fi = pi / 4;
	if (fi > 5 * pi / 4 && fi < 7 * pi / 4)
		fi = 5 * pi / 4;
	return (c * c * sin(2 * fi) / 4);
}

double Bern::allarea(void) const			// ������� ������
{
	return (c * c);
}