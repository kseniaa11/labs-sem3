#include "pch.h"
#include "C:\Users\urkae\Desktop\������������\c��3\git\laba2\laba2\lem.cpp"
#include <math.h>



TEST(BernConstruct, Test_constructor_1)
{
	Bern B;
	ASSERT_DOUBLE_EQ(1, B.get());
}

TEST(BernConstruct, Test_constructor_2)
{
	EXPECT_ANY_THROW(Bern b(0));
	EXPECT_ANY_THROW(Bern b(-1));
	EXPECT_ANY_THROW(Bern b(-2));
}

TEST(BernMethods, Test_set_1)
{
	Bern B;
	B.set(2);
	ASSERT_DOUBLE_EQ(2, B.get());
	B.set(3);
	ASSERT_DOUBLE_EQ(3, B.get());
	B.set(4);
	ASSERT_DOUBLE_EQ(4, B.get());
}

TEST(BernMethods, Test_set_2)
{
	Bern x(1);
	EXPECT_ANY_THROW(x.set(0));
	EXPECT_ANY_THROW(x.set(-1));
	EXPECT_ANY_THROW(x.set(-2));
}

TEST(BernMethods, Test_ro_1)
{
	Bern B(2);
	ASSERT_DOUBLE_EQ(2 * sqrt(2 * cos(2 * pi)), B.ro(pi));
	ASSERT_DOUBLE_EQ(2 * sqrt(2 * cos(2 * pi / 6)), B.ro(pi / 6));
	B.set(5);
	ASSERT_DOUBLE_EQ(5 * sqrt(2 * cos(2 * pi)), B.ro(pi));
	ASSERT_DOUBLE_EQ(5 * sqrt(2 * cos(2 * pi / 6)), B.ro(pi / 6));
}

TEST(BernMethods, Test_ro_2)
{
	Bern B(2);
	EXPECT_ANY_THROW(B.ro(pi/2));
	EXPECT_ANY_THROW(B.ro(3*pi/2));
	B.set(5);
	EXPECT_ANY_THROW(B.ro(pi / 2));
	EXPECT_ANY_THROW(B.ro(3 * pi / 2));
}

TEST(BernMethods, Test_rad1_1)
{
	Bern B(2);
	double r;
	r = 2 * sqrt(2 * cos(2 * pi));
	ASSERT_DOUBLE_EQ(r / (3 * cos(2 * pi)) , B.rad1(pi));
	r = 2 * sqrt(2 * cos(2 * pi/6));
	ASSERT_DOUBLE_EQ(r / (3 * cos(2 * pi/6)), B.rad1(pi/6));
	B.set(5);
	r = 5 * sqrt(2 * cos(2 * pi));
	ASSERT_DOUBLE_EQ(r / (3 * cos(2 * pi)), B.rad1(pi));
	r = 5 * sqrt(2 * cos(2 * pi / 6));
	ASSERT_DOUBLE_EQ(r / (3 * cos(2 * pi / 6)), B.rad1(pi / 6));
}

TEST(BernMethods, Test_rad1_2)
{
	Bern B(2);
	EXPECT_ANY_THROW(B.rad1(pi/2));
	EXPECT_ANY_THROW(B.rad1(3 * pi/2));
	B.set(5);
	EXPECT_ANY_THROW(B.rad1(pi / 2));
	EXPECT_ANY_THROW(B.rad1(3 * pi / 2));
}

TEST(BernMethods, Test_rad2_1)
{
	Bern B(2);
	ASSERT_DOUBLE_EQ(8 / (3 * 1.5), B.rad2(1.5));
	ASSERT_DOUBLE_EQ(8 / (3 * 2.5), B.rad2(2.5));
	B.set(5);
	ASSERT_DOUBLE_EQ(50 / (3 * 4.5), B.rad2(4.5));
	ASSERT_DOUBLE_EQ(50 / (3 * 5.5), B.rad2(5.5));
}


TEST(BernMethods, Test_rad2_2)
{
	Bern B(2);
	EXPECT_ANY_THROW(B.rad2(-20));
	EXPECT_ANY_THROW(B.rad2(20));
	B.set(5);
	EXPECT_ANY_THROW(B.rad2(-100));
	EXPECT_ANY_THROW(B.rad2(100));
}

TEST(BernMethods, Test_area_1)
{
	Bern B(2);
	ASSERT_DOUBLE_EQ((4 * sin(2 * pi) / 4), B.area(pi));
	ASSERT_DOUBLE_EQ((4 * sin(2 * pi/6) / 4), B.area(pi/6));
	B.set(5);
	ASSERT_DOUBLE_EQ((25 * sin(2 * pi) / 4), B.area(pi));
	ASSERT_DOUBLE_EQ((25 * sin(2 * pi / 6) / 4), B.area(pi / 6));
}


TEST(BernMethods, Test_allarea_1)
{
	Bern B(2);
	ASSERT_DOUBLE_EQ(4, B.allarea());
	B.set(3);
	ASSERT_DOUBLE_EQ(9, B.allarea());
	B.set(5);
	ASSERT_DOUBLE_EQ(25, B.allarea());
}


int main(int argc, char* argv[]) 
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}