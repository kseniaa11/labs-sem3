#include <iostream>
#include "help.h"

using std::cin;
using std::cout;
using std::endl;
using namespace helpme;

/*
������������ 1. ������� 13.
�� �������� ������ �������� ������������� ������ ����������� ������� ����� ����� [aij], i = 1, �, m, j = 1, �, n.
�������� m � n ������� �� �������� � �������� �� �������� ������.
������������ ������, i-�� ������� �������� ����� ���������� ������ ��������� � i-�� ������ �������.
�������� ������� � ���������� ������ ������� � �������� ����� � ������������ �������������.
*/

int main()
{
	struct node** matrix;
	int* m;
	if (int n = cre_matrix(matrix, m))
    {
		int* v;
		cre_vector(matrix, v, n, m);
		deletem(matrix, n);
		deletev(v);
	}
	return 0;
}
