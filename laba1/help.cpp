#include "help.h"
#include <iostream>
using std::cin;
using std::cout;
using std::endl;

int number(int& a)
{
	cin >> a;
	while (!cin.good())
	{
		cin.clear();
		char str[120];
		cin.getline(str, 120);
		cin.clear();
		cin >> a;
	}
	return 1;
}

int cre_matrix(struct node**& mass, int* m) // ���� ������� (������ �������)
{
	cout << "Enter number of rows.\n";
	int n = 0;
	number(n);//���������� �����
	cout << "Enter number of elements in row.\n";
	int t = 0;
	number(t);//���������� �����
	m = &t;
    mass = new struct node* [n];// �������� �������
	for (int i = 0; i < n; i++)
	{
		mass[i] = new struct node;
        mass[i]->next = nullptr;// ��������� �������
	}
	int i = -1;
	int j = 0;
	cout << "Enter the number of row (i). Enter -1 for the end.\n";// ����� ������ ��� �������
	number(i);
	while (i>=0 && i<n)
	{
		cout << "Enter the number of column (j).\n";
		number(j);
		if (j >= 0 && j<*m) // �������� �� j � ���������� �������� � �� �������
        {
			if (mass[i]->next == nullptr) // �������� ������� �������� � ������
			{
			    int le;
			    cout << "Enter the element (not 0).\n";

				number(le);
				if (le == 0)
                    {
						cout << "Wrong number. For zero (0) leave position empty.\n";
						cout << "Enter the number of row (i). -1 for the end.\n"; //������� � ������ �������� ��� �����
                        number(i);
						continue;
					}

                mass[i]->next = new struct node;
				mass[i]->next->next = nullptr;
				mass[i]->next->j = j;
				mass[i]->next->a = le;

			}
			else // ������ �� ������
			{
				struct node* pt = mass[i];
				bool flag = false;

				int el;
				cout << "Enter the number (not 0).\n";
                number(el);
                if (el == 0)
                    {
						cout << "Wrong number. For zero (0) leave position empty.\n";
						cout << "Enter the number of row (i). -1 for the end.\n"; //������� � ������ �������� ��� �����
                        number(i);
						continue;
					}

				while (pt->next != nullptr)
				{
					if (pt->next->j == j)
                    {
						cout << "This position (j) isnt free.\n";
						flag = true;
						break;
					}

					 if (pt->next->j>j)
                        break;

					pt = pt->next;
				}

				if (flag==false) //���� �������� �� ������ �� �������
				{
				    struct node* tmp = pt->next;
                    pt->next = new struct node;
					pt->next->next = tmp;
					pt->next->j = j;
					pt->next->a = el;
				}
			}
		}
		else
		{
			cout << "Wrong count of j.\n";
		}
		cout << "Enter the number of row (i). -1 for the end.\n"; //������� � ������ �������� ��� �����
		number(i);
	}
	printm(mass, n, m);
	return n;//��������� �����
}

void cre_vector(struct node** matrix, int* vect, int n, int* m)
{
    int i,hl,s;

    vect = new int[n];//��� ����� �������� ������
    for (i=0;i<n;i++)
        vect[i] = 0;

    for (i=0;i<n;i++)//��� ������ ������
    {
        hl = 0; //���������� ��������� ��������� � ������
        struct node* ptr = matrix[i];
        bool one = false;
        if (ptr->next!=nullptr)
            one = true;//���� ���� �� ���� ������� �� 0
        while (ptr->next!=nullptr)
        {
            hl+=1;
            struct node* tmp = matrix[i];
            bool repet = false;
            while (tmp->next!=ptr->next && tmp->next!=nullptr)
            {
                if (ptr->next->a == tmp->next->a)//���� ����� ���������
                {
                    if (ptr->next->j != tmp->next->j)//�� ��� ���� ����� �� ������ ��������
                    {
                        repet = true; //���� ���������� ���
                        break;
                    }
                }
                tmp = tmp->next;
            }

            if (repet == false)//���� ���������� �� ����
            {
                vect[i] += 1;//����� � ������� �������������
            }

            ptr = ptr->next;
        }
        if (hl<*m && hl>0)//���� � ������ ��� ���� � ��� �������� ����
        {
            vect[i]+=1;
            cout<<"aaaAAA";
        }

        if (one == false)//���� ��� ��������� (�� ������ ����)
            vect[i] = 1;

    }


    cout << "Vector:\t";
    for (i=0;i<n;i++)
    {
        cout << vect[i] << "\t";
    }
    //cout << "vect2";
    return;
}

void printm(struct node** matrix, int n, int* m)
{
	for (int i = 0; i < n; i++)
	{
		struct node* p = matrix[i];
		if (p->next)
        {
			int k1 = 0;
			while (p->next)
			{
				for (int k=k1;k<p->next->j;k++)//����� �� ������� �����
				{
					cout << "0\t";
				}
				cout << p->next->a << "\t";
				k1 = p->next->j+1;
				p = p->next;
			}
			for (int k=k1;k<*m;k++)// ����� ����� ���������� �����
			{
				cout << "0\t";
			}
			cout << "\n";
		}
		else // ����� ���� �����
		{
			for (int k=0;k<*m;k++)
			{
				cout << "0\t";
			}
			cout << "\n";
		}
	}
	cout << "\n";
	return;
}

/*void printv(int* vect, int n)
{
	cout << "Vector:\t";
	for (int i=0;i<n;i++)
	{
	    //cout << "vuvod ";
		cout << vect[i] << "\t";
	}
	cout << "\n";
	return;
}*/

void deletem(struct node** matrix, int n)
{
	for (int i = 0; i < n; i++)
	{
		struct node* p = matrix[i];
		struct node* prev;
		while (p->next != nullptr)
        {
			prev = p;
			p = p->next;
			delete(prev);
		}

	}
	delete [] matrix;
	return;
}

void deletev(int* vect)
{
	delete [] vect;
	return;
}
