#ifndef HELP_H_INCLUDED
#define HELP_H_INCLUDED

#include <iostream>

namespace helpme {
	struct node {
		int j, a;
		struct node* next;
	};
}

using namespace helpme;
int cre_matrix(struct node**& mass, int* m);
void cre_vector(struct node** matrix, int* vect, int n, int* m);
void printm(struct node** matrix, int n, int* m);
//void printv(int* vect, int n);
void deletem(struct node** matrix, int n);
void deletev(int* vect);

#endif // HELP_H_INCLUDED
