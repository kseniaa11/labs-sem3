﻿// laba3.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "Hash.h"
#include <cstring>
#include <stdio.h>
#include <iostream>



    int main()
    {
        Hash h;
        h.printHash();
        return 1;
    }

/* Вариант 18

1. Разработать класс "перемешанная таблица" в соответствии со следующим заданием :
    Состояние класса -
    Таблица представляется в виде вектора(массива), состоящего из элементов.
    Элемент таблицы состоит из ключа(тип int), поля занятости(тип int) и информации(тип char[] фиксированной дли - ны).
    Для описания элемента таблицы целесообразно использовать структуру.Память под массив выделяется статически, во время компиляции, 
    и задается массивом фиксированного размера.
    Преобразование ключа в индекс выполняется функцией хеширования.Элементы перемешиваются методом сложения с константой.
Протокол класса
    Определяет  возможности создания и инициализации экземпляров класса и правила их исполь - зования(методы класса).
Предусмотреть следующие возможности :
    • создание экземпляров структуры(элемента таблицы) с инициализацией начальным состоянием по умолчанию;
    • пустой конструктор для инициализации экземпляров и массивов экземпляров класса(таб - лицы) по умолчанию;
    • создание экземпляров класса(таблицы) с инициализацией заданным количеством элементов из массива ключей и информации;
    • ввод значения экземпляра структуры(элемента таблицы) из входного потока(с помощью перегруженного оператора >> );
    • вывод таблицы в выходной поток(с помощью перегруженного оператора << );
    • поиск элемента таблицы по ключу(с помощью перегруженного оператора());
    • добавление элемента в таблицу(с помощью перегруженного оператора += );
    • выборка информации из таблицы по заданному ключу(с помощью перегруженного оператора[]);
    • удаление элемента из таблицы(с отметкой в поле занятости) по ключу(с помощью пере - груженного оператора -= );
• чистка таблицы от “удаленных элементов” – реорганизация таблицы.
2. Проектирование класса рекомендуется начать с представления состояния класса, 
    учитывающего заданные операции, а затем реализации конструкторов и перегруженного оператора вывода.
    Для отладки и исчерпывающего тестирования других методов разработанного класса реализовать диалоговую программу, 
    которая позволяет вводить параметры, отлаживаемых методов.
    Для обработки ошибочных ситуаций использовать механизм исключительных ситуаций.
3. Повторить разработку класса при условии, что память под строку символов в элементе таблицы и 
    массив структур необходимой длины выделяется динамически, во время выполнения про - граммы(с помощью оператора new; 
    память задается указателем на char в структуре и указателем на структуру в состоянии класса).
Дополнить интерфейс класса следующими возможностями :
    • память под данные поля информации выделять динамически с помощью оператора new;
    • создание экземпляра класса(таблицы) с его инициализацией другим экземпляром класса(копирующий конструктор) для элемента таблицы и таблицы;
    • переопределение экземпляра класса(с помощью перегруженного оператора присваивания) для элемента таблицы и таблицы.
4. Написать прикладную программу, использующую разработанный класс.*/